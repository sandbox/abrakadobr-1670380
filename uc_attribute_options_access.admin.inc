<?php
 
function uc_attribute_options_access_admin_settings() {
    $result = db_query('SELECT * FROM {role}');
	while ($role = db_fetch_object($result)) {
		$roles[$role->rid] = $role;
	}
	$result = db_query('SELECT * FROM {uc_attributes}');
	while ($attr = db_fetch_object($result)) {
		$attributes[$attr->aid] = $attr;
	}
	$result = db_query('SELECT * FROM {uc_attribute_options}');
	while ($opt = db_fetch_object($result)) {
		$options[$opt->aid][$opt->oid] = $opt;
	}
	$result = db_query("SELECT * FROM {uc_attribute_options_access} WHERE nid = %d",0);
	while ( $ent = db_fetch_object($result) ) {
		$dbent[ $ent->rid ][ $ent->aid ][ $ent->oid ] = 1;
	}
	
	foreach ( $roles as $rid => $role ) {
		$form[$role->rid] = array(
			'#type' => 'fieldset',
			'#title' => t( $role->name ),
			'#tree' => TRUE,
		);
		foreach ( $attributes as $aid => $attr ) {
			$form[$role->rid][$attr->aid] = array(
				'#type' => 'fieldset',
				'#title' => $attr->label . ' (' . $attr->name . ')' . ' <i>' . $attr->description . '</i>',
				'#tree' => TRUE,
			);
			foreach( $options[$aid] as $oid => $opt ) {
				$form[$role->rid][$attr->aid][$oid] = array(
					'#type' => 'checkbox',
					'#title' => $opt->name,
				);
				if ( $dbent[$role->rid][$attr->aid][$oid] )
					$form[$role->rid][$attr->aid][$oid]['#default_value'] = 'checked';
			}
		}
	}

	$form['nid'] = array(
		'#type' => 'hidden',
		'#value' => '0',
	);
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save'),
	);
	$form['reset'] = array(
		'#type' => 'reset',
		'#value' => t('Reset'), 
	);
	
	
	return $form;
 }
 
function uc_attribute_options_access_admin_settings_submit($form, &$form_state) {
	//echo "<code><pre>" . var_dump($form_state) . "</pre></code>";
	$dbres;
	$ent = 0;
	$nid = $form_state['values']['nid'] + 0;
	foreach ( $form_state['values'] as $rid => $attr ) {
		if ( is_array( $attr ) ) {
			foreach ( $attr as $aid => $options ) {
				foreach ( $options as $oid => $val ) {
					if ( $val ) {
						$dbres[$ent]['rid'] = $rid;
						$dbres[$ent]['aid'] = $aid;
						$dbres[$ent]['oid'] = $oid;
						$dbres[$ent]['nid'] = $nid;
						$ent++;
					}
				}
			}
		}
	}
	db_query("DELETE FROM {uc_attribute_options_access} WHERE nid = %d",$nid);
	if ( $dbres ) {
		foreach ( $dbres as $id => $entry ) {
			db_query("INSERT INTO {uc_attribute_options_access} (nid,rid,aid,oid) VALUES(%d,%d,%d,%d)",$entry['nid'],$entry['rid'],$entry['aid'],$entry['oid']);
		}
	}
}

function uc_attribute_options_access_node_settings($form_state, $object, $type, $view = 'overview') {
	$overrides = variable_get('uc_attribute_options_access_overrides',array());
	$override = false;
	if ( count($overrides) ) {
		if ( $overrides[ $object->nid ] ) $override = true;
	}
	if ( $override ) $qnid = $object->nid; else $qnid = 0;
	//var_dump($object->nid);
    $result = db_query('SELECT * FROM {role}');
	while ($role = db_fetch_object($result)) {
		$roles[$role->rid] = $role;
	}
	$result = db_query('SELECT * FROM {uc_attributes}');
	while ($attr = db_fetch_object($result)) {
		$attributes[$attr->aid] = $attr;
	}
	$result = db_query('SELECT * FROM {uc_attribute_options}');
	while ($opt = db_fetch_object($result)) {
		$options[$opt->aid][$opt->oid] = $opt;
	}
	$result = db_query("SELECT * FROM {uc_attribute_options_access} WHERE nid = %d",$qnid);
	while ( $ent = db_fetch_object($result) ) {
		$dbent[ $ent->rid ][ $ent->aid ][ $ent->oid ] = 1;
	}
	
	$form['override_defaults'] = array(
		'#type' => 'checkbox',
		'#description' => t('Must be checked id you want use setting for this node.'),
		'#title' => t('Override defaults.'),
	);
	if ( $override ) $form['override_defaults']['#default_value'] = 'checked';
	
	foreach ( $roles as $rid => $role ) {
		$form[$role->rid] = array(
			'#type' => 'fieldset',
			'#title' => t( $role->name ),
			'#tree' => TRUE,
		);
		foreach ( $attributes as $aid => $attr ) {
			$form[$role->rid][$attr->aid] = array(
				'#type' => 'fieldset',
				'#title' => $attr->label . ' (' . $attr->name . ')' . ' <i>' . $attr->description . '</i>',
				'#tree' => TRUE,
			);
			foreach( $options[$aid] as $oid => $opt ) {
				$form[$role->rid][$attr->aid][$oid] = array(
					'#type' => 'checkbox',
					'#title' => $opt->name,
				);
				if ( $dbent[$role->rid][$attr->aid][$oid] )
					$form[$role->rid][$attr->aid][$oid]['#default_value'] = 'checked';
			}
		}
	}

	$form['nid'] = array(
		'#type' => 'hidden',
		'#value' => $object->nid,
	);
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save'),
	);
	$form['reset'] = array(
		'#type' => 'reset',
		'#value' => t('Reset'), 
	);
	
	
	return $form;
}

function uc_attribute_options_access_node_settings_submit($form, &$form_state) {
	//echo "<code><pre>" . var_dump($form_state) . "</pre></code>";
	$nid = $form_state['values']['nid'] + 0;

	if ( !$form_state['values']['override_defaults'] ) {
		$overrides = variable_get('uc_attribute_options_access_overrides',array());
		if ( $overrides[$nid] ) {
			unset($overrides[$nid]);
			variable_set('uc_attribute_options_access_overrides',$overrides);
		}
		db_query("DELETE FROM {uc_attribute_options_access} WHERE nid = %d",$nid);
		return;
	}
	
	$overrides = variable_get('uc_attribute_options_access_overrides',array());
	if ( !$overrides[$nid] ) {
		$overrides[$nid] = 1;
		variable_set('uc_attribute_options_access_overrides',$overrides);
	}
	$dbres;
	$ent = 0;
	foreach ( $form_state['values'] as $rid => $attr ) {
		if ( is_array( $attr ) ) {
			foreach ( $attr as $aid => $options ) {
				foreach ( $options as $oid => $val ) {
					if ( $val ) {
						$dbres[$ent]['rid'] = $rid;
						$dbres[$ent]['aid'] = $aid;
						$dbres[$ent]['oid'] = $oid;
						$dbres[$ent]['nid'] = $nid;
						$ent++;
					}
				}
			}
		}
	}
	db_query("DELETE FROM {uc_attribute_options_access} WHERE nid = %d",$nid);
	if ( $dbres ) {
		foreach ( $dbres as $id => $entry ) {
			db_query("INSERT INTO {uc_attribute_options_access} (nid,rid,aid,oid) VALUES(%d,%d,%d,%d)",$entry['nid'],$entry['rid'],$entry['aid'],$entry['oid']);
		}
	}
}